from flask import Flask
app = Flask(__name__)

@app.route("/")
def hello():
    color = "PowderBlue"
    version = "v0.2.5"
    body = "<html><body style='background-color:%s'>" \
         + '<center><h1>Welcome from Rancher! This is version %s</h1></center></body></html>'
    return body %(color, version)

if __name__ == "__main__":
    app.run(host='0.0.0.0')
