FROM python:3.7-rc-alpine

COPY . /python-app
WORKDIR /python-app

RUN pip install -r requirements.txt

ENTRYPOINT python rancher.py
